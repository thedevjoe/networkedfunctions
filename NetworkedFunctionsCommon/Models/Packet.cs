﻿namespace NetworkedFunctionsCommon.Models
{
    public struct Packet
    {
        public int Version;
        public int OpCode;
        public int Nonce;
        public byte[] Data;
    }
}
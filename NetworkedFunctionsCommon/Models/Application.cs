﻿using System.Collections.Generic;

namespace NetworkedFunctionsCommon.Models
{
    public struct Application
    {
        public string Name;
        public Dictionary<string, string[]> Functions;
    }
}
﻿using System.Net;

namespace NetworkedFunctionsCommon.Models
{
    public struct Connection
    {
        public string Identifier;
        public IPEndPoint Location;
        public Application[] Applications;
    }
}
﻿namespace NetworkedFunctionsCommon
{
    public class OpCode
    {
        private OpCode()
        {
        }

        public const int LOCAL_HELO = 2;
        public const int LOCAL_PUBLISH = 3;
        public const int LOCAL_REPLY = 4;
        public const int LOCAL_CALL = 5;
        public const int LOCAL_ACK = 6;
        public const int LOCAL_PREFETCH = 11;

        public const int OPCODE_EXCHANGE = 1;
        public const int OPCODE_PUSH = 7;
        public const int OPCODE_CALL = 8;
        public const int OPCODE_REPLY = 9;
        public const int OPCODE_ACK = 10;

        public const int ERROR_INVALID_OPCODE = 500;

        public static string GetName(int opcode)
        {
            switch (opcode)
            {
                case OPCODE_EXCHANGE:
                    return "EXCHANGE";
                case OPCODE_ACK:
                    return "ACK";
                case OPCODE_CALL:
                    return "CALL";
                case OPCODE_PUSH:
                    return "PUSH";
                case OPCODE_REPLY:
                    return "REPLY";
                
                case LOCAL_HELO:
                    return "L_HELO";
                case LOCAL_PUBLISH:
                    return "L_PUBLISH";
                case LOCAL_CALL:
                    return "L_CALL";
                case LOCAL_REPLY:
                    return "L_REPLY";
                case LOCAL_ACK:
                    return "L_ACK";
                case LOCAL_PREFETCH:
                    return "L_PREFETCH";
                
                case ERROR_INVALID_OPCODE:
                    return "ERROR_INVALID_OPCODE";
                default:
                    return "UNKNOWN";
            }
        }
        
    }
}
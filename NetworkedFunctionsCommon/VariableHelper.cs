﻿using System;
using System.IO;

namespace NetworkedFunctionsCommon
{
    public static class VariableHelper
    {
        public static void WriteObjectToBinaryStream(this object j, BinaryWriter w)
        {
            if(j is bool x) w.Write(x);
            else if(j is byte x1) w.Write(x1);
            else if(j is char x3) w.Write(x3);
            else if(j is decimal x5) w.Write(x5);
            else if(j is double x6) w.Write(x6);
            else if(j is int x8) w.Write(x8);
            else if(j is long x9) w.Write(x9);
            else if(j is sbyte x10) w.Write(x10);
            else if(j is short x11) w.Write(x11);
            else if(j is string x12) w.Write(x12);
            else if(j is uint x13) w.Write(x13);
            else if(j is ulong x14) w.Write(x14);
            else if(j is ushort x15) w.Write(x15);
        }

        public static object ReadObjectFromBinaryStream<T>(this BinaryReader r)
        {
            if (typeof(T) == typeof(bool)) return r.ReadBoolean();
            else if (typeof(T) == typeof(byte)) return r.ReadByte();
            else if (typeof(T) == typeof(char)) return r.ReadChar();
            else if (typeof(T) == typeof(decimal)) return r.ReadDecimal();
            else if (typeof(T) == typeof(double)) return r.ReadDouble();
            else if (typeof(T) == typeof(int)) return r.ReadInt32();
            else if (typeof(T) == typeof(long)) return r.ReadUInt64();
            else if (typeof(T) == typeof(sbyte)) return r.ReadSByte();
            else if (typeof(T) == typeof(short)) return r.ReadInt16();
            else if (typeof(T) == typeof(string)) return r.ReadString();
            else if (typeof(T) == typeof(uint)) return r.ReadUInt32();
            else if (typeof(T) == typeof(ulong)) return r.ReadUInt64();
            else if (typeof(T) == typeof(ushort)) return r.ReadUInt16();
            return null;
        }
    }
}
﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctionsCommon.Serialization
{
    public static class PacketSerializer
    {
        public static Packet NextPacketInStream(Stream str)
        {
            using (BinaryReader reader = new BinaryReader(str, Encoding.Default, true))
            {
                int version = reader.ReadInt32();
                int opcode = reader.ReadInt32();
                int nonce = reader.ReadInt32();
                int length = reader.ReadInt32();
                byte[] buffer = reader.ReadBytes(length);
                return new Packet()
                {
                    Data = buffer,
                    Nonce = nonce,
                    OpCode = opcode,
                    Version = version
                };
            }
        }

        public static Packet DeserializePacket(this byte[] b)
        {
            using (MemoryStream mem = new MemoryStream(b))
            {
                return NextPacketInStream(mem);
            }
        }

        public static byte[] Serialize(this Packet p)
        {
            using(MemoryStream str = new MemoryStream(p.Data.Length + 9))
            using (BinaryWriter wr = new BinaryWriter(str))
            {
                wr.Write(NFCommon.ProtocolVersion);
                wr.Write(p.OpCode);
                wr.Write(p.Nonce);
                wr.Write(p.Data.Length);
                wr.Write(p.Data);
                return str.ToArray();
            }
        }
    }
}
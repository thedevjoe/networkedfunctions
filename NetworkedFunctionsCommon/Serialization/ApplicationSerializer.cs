﻿using System.Collections.Generic;
using System.IO;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctionsCommon.Serialization
{
    public static class ApplicationSerializer
    {
        public static byte[] Serialize(this Application app)
        {
            using (MemoryStream str = new MemoryStream(1024 * 5))
            {
                using (BinaryWriter wr = new BinaryWriter(str))
                {
                    wr.Write(app.Name);
                    wr.Write(app.Functions.Count);
                    foreach (var fun in app.Functions)
                    {
                        var name = fun.Key;
                        wr.Write(name);
                        wr.Write(fun.Value.Length);
                        for (int i = 0; i < fun.Value.Length; i++)
                        {
                            wr.Write(fun.Value[i]);
                        }
                    }

                    return str.ToArray();
                }
            }
        }

        public static Application DeserializeApplication(this byte[] b)
        {
            using(MemoryStream str = new MemoryStream(b))
            using (BinaryReader reader = new BinaryReader(str))
            {
                string name = reader.ReadString();
                Application app = new Application()
                {
                    Name = name,
                    Functions = new Dictionary<string, string[]>()
                };
                int functions = reader.ReadInt32();
                for (int i = 0; i < functions; i++)
                {
                    string fname = reader.ReadString();
                    int ps = reader.ReadInt32();
                    string[] ptypes = new string[ps];
                    for (int ii = 0; ii < ps; ii++)
                    {
                        ptypes[i] = reader.ReadString();
                    }
                    app.Functions.Add(fname, ptypes);
                }

                return app;
            }
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using NetworkedFunctionsDotnet;

namespace ExampleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            NetworkedFunctions.Init(true);
            Task.Delay(-1).GetAwaiter().GetResult();
        }

        [Networked]
        public static void TestNetworked(char[] c, bool should)
        {
            
        }
    }
}
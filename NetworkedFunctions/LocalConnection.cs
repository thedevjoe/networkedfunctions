﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NetworkedFunctions.Handlers;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;

namespace NetworkedFunctions
{
    public class LocalConnection
    {
        private static int CID = 0;
        private TcpClient _client;
        private int _connectionId;
        public Application _associatedApp;

        public static Dictionary<int, IHandler<LocalConnection>> handlers = new Dictionary<int, IHandler<LocalConnection>>();

        public LocalConnection(TcpClient c)
        {
            _client = c;
            _connectionId = CID++;
        }

        public async Task Run(bool client = false)
        {
            try
            {
                await Thr();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Debug.WriteLine(e.StackTrace);
                l("Closed connection.");
                if(_client.Connected) _client.Close();
                handlers.Clear();
                Program.Applications.Remove(_associatedApp);
            }
        }

        internal void l(string s)
        {
            Console.WriteLine($"[L{_connectionId}/{_associatedApp.Name??"Unknown"}@{(_client.Connected ? ((IPEndPoint)_client.Client.RemoteEndPoint).Port.ToString() : "")}] {s}");
        }

        private async Task Thr()
        {
            var stream = _client.GetStream();
            l("Initiating TCP Connection with " + ((IPEndPoint)_client.Client.RemoteEndPoint).Port.ToString());
            using(MemoryStream str = new MemoryStream(1024 * 5))
            using (BinaryWriter w = new BinaryWriter(str))
            {
                w.Write(Program.Version);

                byte[] pack = (new Packet()
                {
                    OpCode = OpCode.LOCAL_HELO,
                    Version = Program.Version,
                    Data = str.ToArray(),
                    Nonce = 0
                }).Serialize();
                await Write(pack);
                l($"Sent HELO. Length {pack.Length}");
            }
            while (_client.Connected)
            {
                Packet p = PacketSerializer.NextPacketInStream(stream);
                l($"RECV {OpCode.GetName(p.OpCode)}v{p.Version}r{p.Nonce}. Data Length {p.Data.Length}");
                if (!handlers.ContainsKey(p.OpCode))
                {
                    byte[] pack = (new Packet()
                    {
                        OpCode = OpCode.ERROR_INVALID_OPCODE,
                        Version = Program.Version,
                        Data = new byte[0],
                        Nonce = 0
                    }).Serialize();
                    await Write(pack);
                    continue;
                }
                IHandler<LocalConnection> handl = handlers[p.OpCode];
                await handl.Handle(this, p);
            }
        }

        public async Task Write(byte[] b)
        {
            var stream = _client.GetStream();
            await stream.WriteAsync(b);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetworkedFunctions.Handlers.Local;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctions
{
    class Program
    {
        public const int Version = 1;
        public static TcpListener TcpListener;
        public static UdpClient Udp;
        public static TcpListener TcpLocal;
        public static X509Certificate2 Certificate;
        public static X509Certificate CaCertificate;
        public static List<Application> Applications = new List<Application>();
        public static List<TcpConnection> Connections = new List<TcpConnection>();
        public static byte SplitByte;

        static void Main(string[] args)
        {
            Console.WriteLine("NetworkedFunctions Client v" + Version);
            Console.WriteLine("Registering Handlers");
            LocalConnection.handlers.Add(OpCode.LOCAL_PUBLISH, new PublishHandler());
            
            SplitByte = Encoding.UTF8.GetBytes("\n")[0];
            if (!Directory.Exists("crt"))
            {
                Console.WriteLine("Generating Self-Signed Certificate");
                RSA rsa = RSA.Create();
                CertificateRequest req = new CertificateRequest("CN=" + System.Environment.MachineName, rsa, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
                Certificate = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(1));
                byte[] exp = Certificate.Export(X509ContentType.Pkcs12);
                Directory.CreateDirectory("crt");
                File.WriteAllBytes("crt/certificate.pfx", exp);
                //byte[] rs = rsa.ExportRSAPrivateKey();
                //File.WriteAllBytes("crt/key.pem", rs);
            }
            else
            {
                Certificate = new X509Certificate2("crt/certificate.pfx");
            }
            Console.WriteLine("*********** CERTIFICATE INFORMATION ***********");
            Console.WriteLine($"Subject: {Certificate.SubjectName.Name}");
            Console.WriteLine($"Issuer: {Certificate.IssuerName.Name}");
            Console.WriteLine($"Thumbprint: {Certificate.Thumbprint}");
            Console.WriteLine($"Valid Thru: {Certificate.NotAfter.ToLongDateString()}");
            Console.WriteLine("***********************************************");

            if (File.Exists("crt/ca.crt"))
            {
                CaCertificate = X509Certificate.CreateFromCertFile("crt/ca.crt");
            }
            Console.WriteLine("Initializing SSL Server");
            TcpListener = new TcpListener(IPAddress.Any, 4848);
            TcpListener.Start();
            _ = RunTcp();
            Console.WriteLine("Initializing TCP Server");
            TcpLocal = new TcpListener(IPAddress.Loopback, 4850);
            TcpLocal.Start();
            _ = RunLocal();
            Console.WriteLine("Sending UDP Broadcast");
            Udp = new UdpClient(4849);
            Udp.EnableBroadcast = true;
            _ = UdpListener();
            using (MemoryStream m = new MemoryStream(4))
            {
                using (BinaryWriter w = new BinaryWriter(m))
                {
                    w.Write(Version);
                    byte[] b = m.ToArray();
                    Udp.Send(b, b.Length, "255.255.255.255", 4849);
                }
            }
            Task.Delay(-1).GetAwaiter().GetResult();
        }

        public static TcpConnection[] WhoIsResponsibleFor(string app)
        {
            List<TcpConnection> cns = new List<TcpConnection>();
            foreach (TcpConnection c in Connections)
            {
                Application ap = c.Applications.First(e => e.Name == app);
                if(ap.Name == app) cns.Add(c);
            }

            return cns.ToArray();
        }

        static async Task UdpListener()
        {
            while (true)
            {
                var res = await Udp.ReceiveAsync();
                using (MemoryStream str = new MemoryStream(res.Buffer))
                {
                    using (BinaryReader r = new BinaryReader(str))
                    {
                        int vers = r.ReadInt32();
                        if (vers != Version)
                        {
                            Console.WriteLine($"Discovered node at {res.RemoteEndPoint.Address}, but refusing to connect due to version mismatch. v" + vers + " != v" + Version);
                            return;
                        }
                        Console.WriteLine($"Discovered node at {res.RemoteEndPoint.Address}.");
                        TcpConnection.GetConnectionForIP(res.RemoteEndPoint.Address);
                    }
                }
            }
        }

        static async Task RunTcp()
        {
            while (true)
            {
                TcpClient c = await TcpListener.AcceptTcpClientAsync();
                TcpConnection cn = new TcpConnection(c);
                _ = cn.Run();
                Connections.Add(cn);
            }
        }
        
        static async Task RunLocal()
        {
            while (true)
            {
                TcpClient c = await TcpLocal.AcceptTcpClientAsync();
                LocalConnection cn = new LocalConnection(c);
                _ = cn.Run();
            }
        }
    }
}
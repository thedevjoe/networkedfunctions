﻿using System;
using System.Threading.Tasks;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;

namespace NetworkedFunctions.Handlers.Local
{
    public class PublishHandler : IHandler<LocalConnection>
    {
        public async Task Handle(LocalConnection parent, Packet pack)
        {
            Application p = pack.Data.DeserializeApplication();
            Application prev = Program.Applications.Find(e => e.Name == p.Name);
            if (prev.Name == p.Name)
            {
                Program.Applications.Remove(prev);
            }
            Program.Applications.Add(p);
            parent._associatedApp = p;
            parent.l("Registered application " + p.Name + " with " + p.Functions.Count + " functions.");
            await TcpConnection.PushAppsList();
            if (pack.Nonce != 0)
            {
                await parent.Write((new Packet()
                {
                    OpCode = OpCode.LOCAL_ACK,
                    Data = new byte[]{},
                    Nonce = pack.Nonce,
                    Version = NFCommon.ProtocolVersion
                }).Serialize());
            }
        }
    }
}
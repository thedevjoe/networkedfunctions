﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctions.Handlers.Local
{
    public class PrefetchHandler : IHandler<LocalConnection>
    {
        public Task Handle(LocalConnection parent, Packet pack)
        {
            using(MemoryStream stream = new MemoryStream(pack.Data))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                string app = reader.ReadString();
                string func = reader.ReadString();
                Application self = Program.Applications.First(e => e.Name == app);
                if (self.Name == app)
                {
                    
                }
            }
        }
    }
}
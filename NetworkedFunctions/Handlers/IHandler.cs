﻿using System.Threading.Tasks;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctions.Handlers
{
    public interface IHandler<T>
    {
        public Task Handle(T parent, Packet pack);
    }
}
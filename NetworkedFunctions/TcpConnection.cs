﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;

namespace NetworkedFunctions
{
    public class TcpConnection
    {
        private static int CID = 0;
        private TcpClient _client;
        private int _connectionId;
        private X509Certificate _remoteCert;
        private SslStream _stream;
        public Application[] Applications;

        private string _remoteCN
        {
            get
            {
                if (_remoteCert == null) return null;
                var spl = _remoteCert.Subject.Split(",");
                foreach (string s in spl)
                {
                    var ss = s.Trim();
                    var spl1 = ss.Split("=");
                    if (spl1[0] == "CN") return spl1[1];
                }

                return null;
            }
        }

        public TcpConnection(TcpClient c)
        {
            _client = c;
            _connectionId = CID++;
        }

        public static TcpConnection GetConnectionForIP(IPAddress ip, bool create = true)
        {
            foreach (var conn in Program.Connections)
            {
                if (((IPEndPoint) conn._client.Client.RemoteEndPoint).Address.Equals(ip)) return conn;
            }

            if (!create) return null;
            Console.WriteLine("Creating new connection to " + ip);
            TcpClient c = new TcpClient();
            c.Connect(ip, 4848);
            TcpConnection con = new TcpConnection(c);
            _ = con.Run(true);
            Program.Connections.Add(con);
            return con;
        }

        public async Task Run(bool client = false)
        {
            try
            {
                await Thr(client);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Debug.WriteLine(e.StackTrace);
                l("Closed connection.");
                if(_client.Connected) _client.Close();
                if (_stream != null) await _stream.DisposeAsync();
                Program.Connections.Remove(this);
            }
        }

        private void l(string s)
        {
            Console.WriteLine($"[{_connectionId}/{_remoteCN??"Unknown"}@{(_client.Connected ? _client.Client.RemoteEndPoint.ToString() : "")}] {s}");
        }

        private async Task Thr(bool client)
        {
            var stream = _client.GetStream();
            _stream = new SslStream(stream, false, UserCertificateValidationCallback);
            l("Initiating SSL Connection with " + _client.Client.RemoteEndPoint.ToString());
            if (!client)
                await _stream.AuthenticateAsServerAsync(Program.Certificate, true,
                    SslProtocols.Tls13 | SslProtocols.Tls12 | SslProtocols.Tls11, false);
            else
                await _stream.AuthenticateAsClientAsync(((IPEndPoint) _client.Client.RemoteEndPoint).Address.ToString(),
                    new X509Certificate2Collection(Program.Certificate),
                    SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13, false);
            _remoteCert = _stream.RemoteCertificate;
            if (_remoteCert.GetPublicKeyString() == Program.Certificate.GetPublicKeyString())
            {
                // Connected to ourselves, disconnect.
                throw new Exception("Connection Aborted.");
            }
            l("Connected to node " + _remoteCert.Subject + " @ " + _client.Client.RemoteEndPoint.ToString());
            using(MemoryStream str = new MemoryStream(1024 * 5))
            using (BinaryWriter w = new BinaryWriter(str))
            {
                w.Write(Program.Applications.Count);
                foreach (var app in Program.Applications)
                {
                    w.Write(app.Serialize());
                }

                byte[] pack = (new Packet()
                {
                    OpCode = OpCode.OPCODE_EXCHANGE,
                    Data = str.ToArray(),
                    Nonce = 0,
                    Version = Program.Version
                }).Serialize();
                await Write(pack);
                l($"Sent EXCHANGE. Length {pack.Length}");
            }
            while (_client.Connected)
            {
                Packet pack = PacketSerializer.NextPacketInStream(_stream);
                l($"RECV {OpCode.GetName(pack.OpCode)}v{pack.Version}r{pack.Nonce}. Data Length {pack.Data.Length}");
            }
        }

        public async Task Write(byte[] b)
        {
            await _stream.WriteAsync(b);
        }

        private bool UserCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyerrors)
        {
            if (Program.CaCertificate == null) return true;
            foreach (var el in chain.ChainElements)
            {
                if (el.Certificate.PublicKey.Equals(Program.CaCertificate.GetPublicKey())) return true;
            }

            return false;
        }

        public static async Task Broadcast(Byte[] byts)
        {
            foreach (TcpConnection conn in Program.Connections)
            {
                await conn.Write(byts);
            }
        }

        public static async Task PushAppsList(TcpConnection conn = null)
        {
            using(MemoryStream str = new MemoryStream(1024 * 5))
            using (BinaryWriter wr = new BinaryWriter(str))
            {
                wr.Write(Program.Applications.Count);
                foreach (Application app in Program.Applications)
                {
                    wr.Write(app.Serialize());
                }
                Packet pack = new Packet()
                {
                    Data = str.ToArray(),
                    Nonce = 0,
                    OpCode = OpCode.OPCODE_PUSH,
                    Version = NFCommon.ProtocolVersion
                };
                byte[] p = pack.Serialize();
                if (conn == null) await Broadcast(p);
                else await conn.Write(p);
            }
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctionsDotnet.Handlers
{
    public class ReplyAckHandler : IHandler
    {
        private static Random _rand = new Random();
        async Task IHandler.Handle(LocalConnection c, Packet packet)
        {
            if (!NetworkedFunctions.Handlers.ContainsKey(packet.Nonce)) return;
            await NetworkedFunctions.Handlers[packet.Nonce].Invoke(c, packet);
            NetworkedFunctions.Handlers.Remove(packet.Nonce);
        }

        internal static int GetNonce(NetworkedFunctions.ReplyHandler handler)
        {
            int nonce = _rand.Next(Int32.MinValue, Int32.MaxValue);
            NetworkedFunctions.Handlers[nonce] = handler;
            return nonce;
        }
    }
}
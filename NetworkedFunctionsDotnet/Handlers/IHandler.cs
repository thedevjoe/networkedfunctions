﻿using System.Threading.Tasks;
using NetworkedFunctionsCommon.Models;

namespace NetworkedFunctionsDotnet.Handlers
{
    internal interface IHandler
    {
        internal Task Handle(LocalConnection c, Packet packet);
    }
}
﻿using System.IO;
using System.Threading.Tasks;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;

namespace NetworkedFunctionsDotnet.Handlers
{
    public class HeloHandler : IHandler
    {
        async Task IHandler.Handle(LocalConnection c, Packet packet)
        {
            byte[] app = NetworkedFunctions.Application.Serialize();
            Packet pack = new Packet()
            {
                OpCode = OpCode.LOCAL_PUBLISH,
                Data = app,
                Nonce = ReplyAckHandler.GetNonce(async (LocalConnection c, Packet p) =>
                {
                    NetworkedFunctions.Log("Handshake with server complete. Application Initialized.");
                }),
                Version = NFCommon.ProtocolVersion
            };
            byte[] toSend = pack.Serialize();
            c.Write(toSend);
        }
    }
}
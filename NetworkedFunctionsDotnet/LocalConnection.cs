﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Emit;
using System.Threading.Tasks;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;
using NetworkedFunctionsDotnet.Handlers;
using OpCode = NetworkedFunctionsCommon.OpCode;

namespace NetworkedFunctionsDotnet
{
    internal class LocalConnection
    {
        private TcpClient _client;
        internal static Dictionary<int, IHandler> Handlers = new Dictionary<int, IHandler>();
        internal LocalConnection()
        {
            
        }

        internal async Task Run(int connattempt = 1)
        {
            if (connattempt > 3)
            {
                await Console.Error.WriteLineAsync("[NF Fatal] Unable to reconnect.");
                return;
            }
            try
            {
                _client = new TcpClient();
                await _client.ConnectAsync(IPAddress.Loopback, 4850);
                connattempt = 1;
                NetworkedFunctions.Log("Connected");
                await Thr();
            }
            catch (Exception e)
            {
                await Console.Error.WriteLineAsync(e.Message);
                await Console.Error.WriteLineAsync(e.StackTrace);
                if(_client.Connected) _client.Close();
                await Console.Error.WriteLineAsync("[NF] Attempting Reconnection. Attempting reconnect in 5s.");
                await Task.Delay(5000);
                _ = Run(connattempt+1);
            }
        }

        internal async Task Thr()
        {
            NetworkedFunctions.Log("Waiting for packets");
            var stream = _client.GetStream();
            while (_client.Connected)
            {
                Packet pack = PacketSerializer.NextPacketInStream(stream);
                NetworkedFunctions.Log($"Got packet of type {OpCode.GetName(pack.OpCode)}. Data Length {pack.Data.Length}");
                if (!Handlers.ContainsKey(pack.OpCode))
                {
                    await Console.Error.WriteLineAsync("[NF Error] Server sent packet of type " +
                                                       OpCode.GetName(pack.OpCode) + "(" + pack.OpCode +
                                                       "), but it was not registered.");
                    continue;
                }

                IHandler h = Handlers[pack.OpCode];
                h.Handle(this, pack);
            }
        }

        internal async Task Write(byte[] b)
        {
            Packet p = b.DeserializePacket();
            NetworkedFunctions.Log($"Sending {OpCode.GetName(p.OpCode)}. Packet length {b.Length}");
            await _client.GetStream().WriteAsync(b);
        }
    }
}
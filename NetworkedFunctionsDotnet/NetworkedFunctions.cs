﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NetworkedFunctionsCommon;
using NetworkedFunctionsCommon.Models;
using NetworkedFunctionsCommon.Serialization;
using NetworkedFunctionsDotnet.Handlers;

namespace NetworkedFunctionsDotnet
{
    public class NetworkedFunctions
    {
        internal static bool DebugMode = false;
        internal static Application Application;
        internal static Dictionary<string, MethodInfo> Methods = new Dictionary<string, MethodInfo>();
        internal static LocalConnection Connection;

        internal delegate Task ReplyHandler(LocalConnection c, Packet p);
        internal static Dictionary<int, ReplyHandler> Handlers = new Dictionary<int, ReplyHandler>();

        public delegate Task FunctionResponse<T>(T data);
        
        internal static void Log(string s)
        {
            Console.WriteLine("[NF Debug] " + s);
        }
        public static void Init(bool debug = false)
        {
            DebugMode = debug;
            // Register Handlers
            LocalConnection.Handlers.Add(OpCode.LOCAL_HELO, new HeloHandler());
            LocalConnection.Handlers.Add(OpCode.LOCAL_REPLY, new ReplyAckHandler());
            LocalConnection.Handlers.Add(OpCode.LOCAL_ACK, new ReplyAckHandler());
            
            // Register Functions
            StackTrace trace = new StackTrace();
            StackFrame frame = trace.GetFrame(1);
            Type caller = frame.GetMethod().DeclaringType;
            Log($"Identified Assembly {caller.Assembly.GetName().Name}");
            Application = new Application()
            {
                Name = caller.Assembly.GetName().Name,
                Functions = new Dictionary<string, string[]>()
            };
            var exp = caller.Assembly.GetTypes().SelectMany(t => t.GetMethods())
                .Where(t => t.GetCustomAttributes(typeof(NetworkedAttribute), false).Length > 0).ToArray();
            foreach (var m in exp)
            {
                Log($"Registering networked function {m.DeclaringType}.{m.Name}");
                var r = m.ReturnType;
                var args = m.GetParameters();
                string[] arr = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    arr[i] = args[i].ParameterType.Name;
                }
                Application.Functions.Add($"{m.DeclaringType}.{m.Name}", arr);
                Methods.Add($"{m.DeclaringType}.{m.Name}", m);
            }
            Log("Connecting to server.");
            Connection = new LocalConnection();
            _ = Connection.Run();
        }

        public static async Task CallFunction<T>(string app, string function, FunctionResponse<T> callback, params object[] vars)
        {
            using(MemoryStream m = new MemoryStream(1024 * 5))
            using (BinaryWriter wr = new BinaryWriter(m))
            {
                wr.Write(app);
                wr.Write(function);
                byte[] b = (new Packet()
                {
                    Data = m.ToArray(),
                    OpCode = OpCode.LOCAL_PREFETCH,
                    Nonce = ReplyAckHandler.GetNonce(async (LocalConnection conn, Packet p) => await _afterPrefetch<T>(app, function, callback, vars, p)),
                    Version = NFCommon.ProtocolVersion
                }).Serialize();
                await Connection.Write(b);
            }
        }

        private static async Task _afterPrefetch<T>(string app, string function, FunctionResponse<T> callback, object[] vars, Packet p)
        {
            string[] x;
            using(MemoryStream stream = new MemoryStream(p.Data))
            using (BinaryReader re = new BinaryReader(stream))
            {
                int param = re.ReadInt32();
                string[] par = new string[param];
                for (int i = 0; i < param; i++)
                {
                    par[i] = re.ReadString();
                }

                x = par;
            }

            if (vars.Length != x.Length)
            {
                await Console.Error.WriteLineAsync($"Application {app} function {function} requires {x.Length} parameters. {vars.Length} given.");
                await Console.Error.WriteLineAsync(new StackTrace().ToString());
                return;
            }
            for (int i=0;i<vars.Length;i++)
            {
                if (vars[i].GetType().Name != x[i])
                {
                    await Console.Error.WriteLineAsync($"Application {app} function {function} requires parameter {i} to be of type {x[i]}. {vars[i].GetType().Name} given.");
                    await Console.Error.WriteLineAsync(new StackTrace().ToString());
                    return;
                }
            }
            using(MemoryStream m = new MemoryStream(1024 * 5))
            using (BinaryWriter wr = new BinaryWriter(m))
            {
                wr.Write(app);
                wr.Write(function);
                wr.Write(vars.Length);
                foreach (object v in vars)
                {
                    v.WriteObjectToBinaryStream(wr);
                }
                byte[] b = (new Packet()
                {
                    Data = m.ToArray(),
                    OpCode = OpCode.LOCAL_CALL,
                    Nonce = ReplyAckHandler.GetNonce(async (LocalConnection conn, Packet p) =>
                    {
                        using(MemoryStream str = new MemoryStream(p.Data))
                        using (BinaryReader reader = new BinaryReader(str))
                        {
                            T result = (T) reader.ReadObjectFromBinaryStream<T>();
                            await callback.Invoke(result);
                        }
                    }),
                    Version = NFCommon.ProtocolVersion
                }).Serialize();
                await Connection.Write(b);
            }
        }
    }
}
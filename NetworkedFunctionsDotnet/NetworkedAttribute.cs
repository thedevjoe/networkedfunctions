﻿using System;
using System.Runtime.InteropServices;

namespace NetworkedFunctionsDotnet
{
    [AttributeUsage(AttributeTargets.Method)]
    public class NetworkedAttribute : Attribute
    {
    }
}